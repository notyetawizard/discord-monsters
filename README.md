# Discord Monsters

A nice, simple text adventure game for discord servers, GMed by a bot.

# Features

#### General
- Six types of monsters, made up of only 3 damage types with no perfect type triangles.
- Each monster gets a sub type which is purely stylistic, and helps distinguish them.
- Types and subtypes are displayed only as emoji.

- Monsters have 4 stats: Health, speed, attack, defense.
- Monsters fight other monsters and attack automatically.
- Monsters gain level up when they win fights. Leveling is on a curve.

- Everyone gets a random level 1 starter.
- Bot tracks everyone's stuff.
- One battle at a time, per channel.
- Battles are automatic once started.

- When no battles are running in a channel, the bot may randomly spawn wild monsters.
- Wild monsters may be captured at low health, or killed for experience.

- Players can challenge anyone or a specific user, and and may accept or decline challenges.
- A level limit may be set, and a number of monsters to be used (max 12, levels averaged).

# Commands


# Monster Types

#### Slime: Kinetic damage, weak to Phychic, resistant to Kinetic (:custard:)
- snake
- poop
- dango
- ice_cream
- rice_ball
- tomato
- honey_pot
- doughnut
- hamburger
- fire

#### Beast: Kinetic damage, weak to Kinetic, resistant to Elemental (:feet:)
- bird
- monkey
- horse2
- ram
- owl
- fox
- cat2
- dog2
- elephant
- tiger2
- ox
- whale

#### Angel: Elemental damage, weak to Phychic, resistant to Elemental (:angel:)
- dragon
- dancer
- bee
- dove
- snowman
- bow_and_arrow
- fencer
- notes
- unicorn

#### Plant: Elemental damage, weak to Elemental, resistant to Kinetic (:herb:)
- cactus
- evergreen_tree
- deciduous_tree
- palm_tree
- seedling
- bamboo
- sunflower
- rose
- tulip
- blossom
- hibiscus
- mushroom

#### Demon: Phychic damage, weak to Kinetic, resistant to Phychic (:imp:)
- dragon
- frog
- snake
- lizard
- crocodile
- rhino
- space_invader
- boxing_glove
- gun
- bat

#### Ghost: Phychic damage, weak to Elemental, resistant to Psychic (:ghost:)
- octopus
- spider
- spy
- owl
- jack_o_lantern
- cloud_tornado
- crossed_swords
- bomb
- bell
- fire

#### Special subtypes
- heart (can heal other monsters)
- gay_pride_flag (won't attack others of the same subtype, will make out instead)
- trophy (super rare, high stat bonus)