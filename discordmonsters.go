package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
)

// Run the thing!
func main() {
	// Load the Token from a file
	Token, err := ioutil.ReadFile("token")
	if err != nil {
		fmt.Println("Error reading the token file:", err)
		return
	}

	// Create the Discord Session
	Discord, err := discordgo.New("Bot " + string(Token))
	if err != nil {
		fmt.Println("Error while creating Discord session:", err)
		return
	}

	// Open a websocket connection to Discord and begin listening.
	err = Discord.Open()
	if err != nil {
		fmt.Println("Error while opening connection to Discord:", err)
		return
	}

	// Register the messageCreate func as a callback for MessageCreate events.
	Discord.AddHandler(messageCreate)

	// initialize global pseudo random generator
	rand.Seed(time.Now().Unix())

	// Wait here until CTRL-C or other term signal is received.
	fmt.Println("Discord Monsters is running! Press Ctrl-C to quit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Close down nicely when done.
	Discord.Close()
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the autenticated bot has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	if m.Author.ID == s.State.User.ID {
		return
	}
	// If the message is "ping" reply with "Pong!"
	if m.Content == "ping" {
		s.ChannelMessageSend(m.ChannelID, "Pong!")
	}

	// If the message is "pong" reply with "Ping!"
	if m.Content == "pong" {
		s.ChannelMessageSend(m.ChannelID, "Ping!")
	}
}
