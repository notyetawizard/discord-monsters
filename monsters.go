package main

import (
	"math"
	"math/rand"
)

var kinds = [6]string{
	"custard",
	"feet",
	"angel",
	"herb",
	"imp",
	"ghost",
}

var kind = map[string][10]string{
	"custard": {
		"snake",
		"dango",
		"ice_cream",
		"rice_ball",
		"tomato",
		"honey_pot",
		"doughnut",
		"hamburger",
		"fire",
		"heart",
	},
	"feet": {
		"bird",
		"monkey",
		"horse2",
		"ram",
		"owl",
		"cat2",
		"dog2",
		"elephant",
		"ox",
		"heart",
	},
	"angel": {
		"dragon",
		"dancer",
		"bee",
		"dove",
		"snowman",
		"bow_and_arrow",
		"fencer",
		"notes",
		"unicorn",
		"heart",
	},
	"herb": {
		"cactus",
		"evergreen_tree",
		"deciduous_tree",
		"palm_tree",
		"seedling",
		"sunflower",
		"rose",
		"hibiscus",
		"mushroom",
		"heart",
	},
	"imp": {
		"dragon",
		"frog",
		"snake",
		"lizard",
		"crocodile",
		"rhino",
		"space_invader",
		"boxing_glove",
		"bat",
		"heart",
	},
	"ghost": {
		"octopus",
		"spider",
		"owl",
		"jack_o_lantern",
		"cloud_tornado",
		"crossed_swords",
		"bomb",
		"bell",
		"fire",
		"heart",
	},
}

type Monster struct {
	name    string
	major   string
	minor   string
	level   int
	wins    int
	health  int
	speed   int
	attack  int
	defense int
}

func (Monster) NewRandom() Monster {
	var monster Monster
	// Unnamed!
	monster.name = "???"
	// Random major and minor type
	monster.major = kinds[rand.Intn(len(kinds))]
	monster.minor = kind[monster.major][rand.Intn(len(kind[monster.major]))]
	// Random health and such
	monster.health = 4 + rand.Intn(6)
	monster.speed = 4 + rand.Intn(6)
	monster.attack = 4 + rand.Intn(6)
	monster.defense = 4 + rand.Intn(6)

	return monster
}

func (m Monster) SetName(n string) {
	m.name = n
}

func (m Monster) AddWin() {
	m.wins += 1
	root := int(math.Floor(math.Sqrt(float64(m.wins) + 1)))
	if root > m.level {
		m.level = root
		m.health += rand.Intn(4)
		m.speed += rand.Intn(4)
		m.attack += rand.Intn(4)
		m.defense += rand.Intn(4)
	}
}

func (m Monster) GetName() string {
	return ":" + m.major + "::" + m.minor + ": " + m.name
}

func (m Monster) GetStats() string {
	return "HP: " + string(m.health) + "\nSPD: " + string(m.speed) +
		"\nATK: " + string(m.attack) + "\nDEF: " + string(m.defense)
}
